# A Potential Build System for datomatic.no-intro.org's Front End

## Background

Weeks ago I saw a message on their website from their admin, asking for help on modernising the website's CSS.

For a long time the website has been very beneficial to me and a lot of ROM collectors, so I decided to give my two cents.

## How to Use

1. Clone this project
1. Run `npm i` in the root folder of this project

### Production Build

Run `npm run build` to obtain the production files in `dist` folder.

Run `npm start` to launch an HTTP server at `localhost:8080` to see the result.

### Development Build

Run `npm run watch` to launch an HTTP server at `localhost:8080`.

Modify `src/index.html` then hard refresh the development page in the browser to see the result.

## Goal

The goal of this project is to make all three CSS files inside `src/css/legacy_classes` deprecated. Once all legacy classes are moved, flex boxes will be leveraged to provide a mobile friendly view.

To style the elements, classes are directly applied to them inside the HTML file, `index.html` using TailwindCSS.

## License

The webpage template, CSS files, and images included belong to their original authors. Please contact datomatic.no-intro.org's admin for detail.

Other files are licensed with MIT license.
